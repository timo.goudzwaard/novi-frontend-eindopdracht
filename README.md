# ToDo app in de cloud

Een simpele, overzichtelijke to-do app die al je items opslaat in de cloud.

Je kan inloggen met een Google account en vervolgens worden je gegevens opgeslagen in Firestore (Firebase).

De to-do items worden realtime bijgehouden, wat betekend dat als je een nieuwe toevoegt of afvinkt, al je apparaten de update krijgen als ze verbonden zijn met het internet.

De website staat live op [firebase](https://to-do-app-novi.web.app/).

De app is ook ontwikkeld zodat ik het kon bekijken op mijn Tesla, zie foto hier onder:
![Foto Tesla](./src/assets/foto-tesla.jpg)

## Pre requirements

Zorg dat je [node](https://nodejs.org/en/) hebt geinstalleerd op je machine. Tijdens het ontwikkel proces heb ik gebruik gemaakt van v10.16.3
Het is aan te raden om niet een veel oudere of nieuwere versie als mijn versie te gebruiken. Dit kan voor problemen zorgen.

Ook helpt het om een goede code editer te gebruiken, ik kan [VSCode](https://code.visualstudio.com/) aanraden.

## Installatie

Na je het project gecloned of gedownload hebt: gebruik het script `npm install` om de packages te installeren

## Project opstarten

Gebruik het volgende commando in je terminal om het project te starten: `npm run start`

Open [http://localhost:3000](http://localhost:3000) om het in je browser te bekijken.

## Project builden

Gebruik `npm run build` om het project te builden.

## Project deployen

Dit project gebruikt firebase om te deployen. Als je firebase tools hebt geinstalleerd en je bent ingelogd op een account dat toegang heeft tot het firebase project dat gebruikt is hiervoor, dan kan je het `npm run deploy` script gebruiken om te builden en automatisch te deployen.

## PWA (Progressive Web App)

Deze applicatie is ook een PWA, hiermee kan je het op je telefoon installeren als een app op je homescreen. Wat een prettige gebruikerservaring geeft.
Om dit te installeren kies je OS:

### Android

Ga op je android device met een chrome browser naar [de website](https://to-do-app-novi.web.app/), en daar krijg je vervolgens deze popup:
![PWA Android](./src/assets/ss-PWA-android.png)

Klik daar op en het wordt toegevoegd op je homescreen als app!

### iOS

Bij iOs is het helaas alleen nog mogelijk om dit via de safari browser te doen. Als je met de safari browser naar [de website](https://to-do-app-novi.web.app/) gaat, klik je op opties en `Add to homescreen` om deze toe te voegen als een app op je homescreen.

## Gebruik van de website/ app

Als je de website of PWA app opstart, zal je eerst het homescreen zien. Deze pagina/scherm wordt gebruikt als landingspagina en kan gebruikt worden om bezoekers aan te sturen om een account te maken.

Je kan nu op inloggen drukken in het menu om in te loggen met een Google account. Het is mogelijk om meer social logins toe te voegen, in dit geval heb ik alleen gebruik gemaakt van Google.

Zodra je bent ingelogd krijg je je ToDo app scherm te zien. Het is onderverdeeld in 3 niveau's van prioriteit. Je kan per prioriteit een nieuwe toevoegen en de individuele items 'afstrepen'

Je kan vervolgens weer uitloggen door op de rode uitlop knop te drukken.

Als je bent ingelogd wordt het d.m.v. een JWT token een tijd bewaard, zodat als je later terug komt naar de website of PWA app je niet direct weer hoeft in te loggen.

## Unit testen

Gebruik `npm run test` om de unit testen te draaien. De applicatie maakt gebruikt van (Jest)[https://jestjs.io/] en (React Testing Library)[https://testing-library.com/docs/react-testing-library/intro]
