import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyAJvFd8YZAyic_ETmvhr2koG_E7zsslDj4',
  authDomain: 'to-do-app-novi.firebaseapp.com',
  databaseURL: 'https://to-do-app-novi.firebaseio.com',
  projectId: 'to-do-app-novi',
  storageBucket: 'to-do-app-novi.appspot.com',
  messagingSenderId: '745154102446',
  appId: '1:745154102446:web:8cd5edb4354ad0b1e7193f',
};

firebase.initializeApp(firebaseConfig);

export default firebase;
