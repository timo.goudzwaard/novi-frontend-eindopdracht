const baseError = 'Er is een probleem met';

const ErrorMessages = {
  initialize: `${baseError} het initialiseren van jouw account.`,
  checkToDo: `${baseError} het afvinken van de to-do.`,
  addToDo: `${baseError} het toevoegen van de to-do.`,
  addXP: `${baseError} het toevoegen van de XP.`,
  addHistory: `${baseError} het toevoegen van de geschiedenis.`,
};

export default ErrorMessages;
