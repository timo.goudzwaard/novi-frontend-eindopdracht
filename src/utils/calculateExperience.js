const baseCalculator = (experience) => {
  return experience / (1000 + (experience / 100) * 5);
};

export const calculateLevel = (experience) => {
  return Math.floor(baseCalculator(experience));
};

export const calculatePercentageToNextLevel = (experience) => {
  const levelWithExperience = baseCalculator(experience);
  const fullLevel = calculateLevel(experience);
  return Math.floor((levelWithExperience - fullLevel) * 100);
};
