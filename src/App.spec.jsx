import { render } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

jest.mock('./components/header/header', () => {
  return 'div';
});

describe('App', () => {
  it('should render App component correctly without child components (mocked)', () => {
    const { container } = render(
      <BrowserRouter>
        <App />
      </BrowserRouter>
    );

    expect(container).toMatchSnapshot();
  });
});
