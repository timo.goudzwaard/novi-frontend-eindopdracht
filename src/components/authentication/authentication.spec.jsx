import { render } from '@testing-library/react';
import React from 'react';
import Authentication from './authentication';

// mocks to make sure the component renders without external issues
jest.mock('react-router-dom', () => ({
  useLocation: jest.fn(),
  useHistory: jest.fn(),
}));
jest.mock('firebase', () => {
  return {
    auth: jest.fn(() => {
      return {
        onAuthStateChanged: jest.fn(),
        signOut: jest.fn(),
      };
    }),
    initializeApp: jest.fn(),
  };
});

xdescribe('Authentication', () => {
  it('should show spinner on render', () => {
    const { container } = render(<Authentication />);

    expect(container).toMatchSnapshot();
  });
});
