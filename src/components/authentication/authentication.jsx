import React, { useEffect, useState } from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import firebase from '../../configs/firebase';
import Button from '../button/button';
import LoginButton from '../login-button/login-button';
import Spinner from '../spinner/spinner';
import './authentication.scss';

const Authentication = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // user is authenticated
        setIsLoading(false);
        setAuthenticated(true);
      } else {
        // user is NOT authenticated
        setIsLoading(false);
        setAuthenticated(false);

        if (location.pathname !== '/') {
          history.push('/');
        }
      }
    });
  }, [location, history]);

  const logout = () => {
    firebase.auth().signOut();
  };

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <div className="authentication">
      {authenticated ? (
        <div className="authentication__logged-in">
          <Link to="/account">
            <Button>Account</Button>
          </Link>

          <Link to="/geschiedenis">
            <Button>Geschiedenis</Button>
          </Link>

          <Button color="secondary" onClick={logout}>
            Log Out
          </Button>
        </div>
      ) : (
        <LoginButton />
      )}
    </div>
  );
};

export default Authentication;
