import { render } from '@testing-library/react';
import React from 'react';
import LevelIndicator from './level-indicator';

jest.mock('firebase', () => {
  return {
    auth: jest.fn(() => {
      return {
        onAuthStateChanged: jest.fn(),
        signOut: jest.fn(),
      };
    }),
    initializeApp: jest.fn(),
  };
});

xdescribe('LevelIndicator', () => {
  it('should render the component', () => {
    const { container } = render(<LevelIndicator />);

    expect(container).toMatchSnapshot();
  });
});
