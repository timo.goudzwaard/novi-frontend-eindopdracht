import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import firebase from '../../configs/firebase';
import {
  calculateLevel,
  calculatePercentageToNextLevel,
} from '../../utils/calculateExperience';
import './level-indicator.scss';

const LevelIndicator = ({ alignLeft }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [experience, setExperience] = useState(0);
  const [hasUser, setHasUser] = useState(false);

  useEffect(() => {
    let dbUnsubscribe;

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const db = firebase.firestore();
        dbUnsubscribe = db
          .collection('users')
          .doc(firebase.auth().currentUser.uid)
          .onSnapshot(
            (doc) => {
              const dbExperience = doc.data().experience;

              setExperience(dbExperience);
              setHasUser(true);
            },
            (error) => {
              console.error(error);
            }
          );
      } else {
        setHasUser(false);
      }

      setIsLoading(false);
    });

    return () => {
      // unsubscribe to database steam
      if (dbUnsubscribe) {
        dbUnsubscribe();
      }
    };
  }, []);

  // don't display anything if user has not authenticated (yet)
  if (isLoading || !hasUser) {
    return null;
  }

  return (
    <div
      className={`level-indicator ${
        alignLeft && 'level-indicator--align-left'
      }`}
    >
      <div className="level-indicator__level">
        {`Level: ${calculateLevel(experience)}`}
      </div>
      <div className="level-indicator__progress">
        <progress
          id="levelPercentage"
          max="100"
          value={calculatePercentageToNextLevel(experience)}
        />
      </div>
    </div>
  );
};

LevelIndicator.propTypes = {
  alignLeft: PropTypes.bool,
};

LevelIndicator.defaultProps = {
  alignLeft: false,
};

export default LevelIndicator;
