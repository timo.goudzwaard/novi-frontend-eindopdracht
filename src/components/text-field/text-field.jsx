import React from 'react';
import PropTypes from 'prop-types';
import './text-field.scss';

const TextField = ({ id, label, value, onChange, className }) => {
  return (
    <label htmlFor={id}>
      {label}
      <input
        id={id}
        value={value}
        className={`text-field ${className}`}
        onChange={(e) => {
          onChange(e);
        }}
      />
    </label>
  );
};

TextField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  className: PropTypes.string,
};

TextField.defaultProps = {
  onChange: () => {},
  className: '',
};

export default TextField;
