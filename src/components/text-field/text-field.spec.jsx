import { render } from '@testing-library/react';
import React from 'react';
import TextField from './text-field';

describe('TextField', () => {
  it('should render the TextField component with the correct ID and label', () => {
    const { container } = render(<TextField id="test" label="test123" />);

    expect(container).toMatchSnapshot();
  });

  it('should add classnames when its passed down as props', () => {
    const { container } = render(
      <TextField id="test" label="test123" className="test1 test2" />
    );

    expect(container).toMatchSnapshot();
  });
});
