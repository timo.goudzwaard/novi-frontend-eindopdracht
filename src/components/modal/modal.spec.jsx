import { render } from '@testing-library/react';
import React from 'react';
import Modal from './modal';

describe('Modal', () => {
  it('should render the modal with the child component', () => {
    const { container } = render(
      <Modal open={false} onClose={() => {}}>
        <h1>Header1</h1>
      </Modal>
    );

    expect(container).toMatchSnapshot();
  });

  it('should add the visible class if open is set to true', () => {
    const { container } = render(
      <Modal open onClose={() => {}}>
        <h2>Header2</h2>
      </Modal>
    );

    expect(container).toMatchSnapshot();
  });
});
