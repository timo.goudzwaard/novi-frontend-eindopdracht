import React from 'react';
import PropTypes from 'prop-types';
import './modal.scss';

const Modal = ({ children, open, onClose }) => {
  const handleKeyDown = (event) => {
    if (event.keyCode === 27) {
      onClose();
    }
  };

  return (
    <div
      role="none"
      onKeyDown={(event) => {
        handleKeyDown(event);
      }}
      className={`modal ${open && 'modal--visible'}`}
    >
      {children}
    </div>
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default Modal;
