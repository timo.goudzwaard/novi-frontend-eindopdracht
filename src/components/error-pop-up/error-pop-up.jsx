import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import './error-pop-up.scss';

const ErrorPopUp = ({ hasError, handleClose, errorMessage }) => {
  useEffect(() => {
    const timer = () => {
      setTimeout(() => {
        handleClose();
      }, 5000);
    };

    if (hasError) {
      timer();
    }
  }, [hasError, handleClose]);

  return (
    <div className="error-pop-up__wrapper">
      <div className={`error-pop-up ${hasError && 'error-pop-up--visible'}`}>
        {errorMessage}
      </div>
    </div>
  );
};

ErrorPopUp.propTypes = {
  hasError: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
};

ErrorPopUp.defaultProps = {
  errorMessage: 'Er is een probleem opgetreden. Probeer het later nog eens.',
};

export default ErrorPopUp;
