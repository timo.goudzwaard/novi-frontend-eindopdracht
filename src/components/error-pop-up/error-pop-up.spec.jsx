import { render } from '@testing-library/react';
import React from 'react';
import ErrorPopUp from './error-pop-up';

describe('errorPopUp', () => {
  it('should not add visible class if error is set to false', () => {
    const { container } = render(
      <ErrorPopUp hasError={false} handleClose={() => null} />
    );

    expect(container).toMatchSnapshot();
  });

  it('should add the visible class if error is set to true', () => {
    const { container } = render(
      <ErrorPopUp hasError handleClose={() => null} />
    );

    expect(container).toMatchSnapshot();
  });
});
