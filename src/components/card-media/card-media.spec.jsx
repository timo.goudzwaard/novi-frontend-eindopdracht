import { render } from '@testing-library/react';
import React from 'react';
import CardMedia from './card-media';
import testImage from '../../assets/geschiedenis.png';

describe('CardMedia', () => {
  it('should render the component with the correct alt text and image', () => {
    const { container } = render(
      <CardMedia alt="test image" image={testImage} />
    );

    expect(container).toMatchSnapshot();
  });

  it('should render the component with the correct alt text', () => {
    const { container } = render(
      <CardMedia alt="test alt text" image={testImage} />
    );

    expect(container).toMatchSnapshot();
  });
});
