import React from 'react';
import PropTypes from 'prop-types';
import './card-media.scss';

const CardMedia = ({ image, alt }) => {
  return (
    <div className="card-media">
      <img className="card-media__img" src={image} alt={alt} />
    </div>
  );
};

CardMedia.propTypes = {
  image: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};

export default CardMedia;
