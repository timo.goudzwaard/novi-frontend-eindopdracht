import { render } from '@testing-library/react';
import React from 'react';
import Container from './container';

describe('Container', () => {
  it('should render the container with child elements', () => {
    const { container } = render(
      <Container>
        <h1>Header</h1>
      </Container>
    );

    expect(container).toMatchSnapshot();
  });
});
