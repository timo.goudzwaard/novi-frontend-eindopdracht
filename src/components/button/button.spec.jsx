import { render } from '@testing-library/react';
import React from 'react';
import Button from './button';

describe('Container', () => {
  it('should render button with the correct text', () => {
    const { container } = render(<Button>knopje</Button>);

    expect(container).toMatchSnapshot();
  });

  it('should render button with the correct text and color', () => {
    const { container } = render(
      <Button color="secondary">nog een knopje</Button>
    );

    expect(container).toMatchSnapshot();
  });
});
