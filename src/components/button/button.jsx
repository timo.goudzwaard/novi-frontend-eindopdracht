import React from 'react';
import PropTypes from 'prop-types';
import './button.scss';

const Button = ({ children, color, onClick }) => {
  return (
    <button
      type="button"
      className={`button button--${color}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.string.isRequired,
  color: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  color: 'primary',
  onClick: () => {},
};

export default Button;
