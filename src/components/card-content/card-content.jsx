import React from 'react';
import PropTypes from 'prop-types';
import './card-content.scss';

const CardContent = ({ children }) => {
  return <div className="card-content">{children}</div>;
};

CardContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default CardContent;
