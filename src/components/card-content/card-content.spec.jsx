import { render } from '@testing-library/react';
import React from 'react';
import CardContent from './card-content';

describe('CardContent', () => {
  it('should render the component with child(ren)', () => {
    const { container } = render(
      <CardContent>
        <h1>Title here</h1>
        <small>subtitle here</small>
      </CardContent>
    );

    expect(container).toMatchSnapshot();
  });
});
