import { render } from '@testing-library/react';
import React from 'react';
import Card from './card';

describe('Card', () => {
  it('should render the card component with child components', () => {
    const { container } = render(
      <Card>
        <h1>Title</h1>
      </Card>
    );

    expect(container).toMatchSnapshot();
  });

  it('should add classname', () => {
    const { container } = render(
      <Card className="testclass">
        <h1>Title</h1>
      </Card>
    );

    expect(container).toMatchSnapshot();
  });
});
