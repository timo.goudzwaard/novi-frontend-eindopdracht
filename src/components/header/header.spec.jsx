import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import React from 'react';
import Header from './header';

// mocks
jest.mock('../authentication/authentication', () => () =>
  'mockedAuthhenticationComponent'
);

describe('header', () => {
  it('should render the header', () => {
    const { container } = render(
      <Router>
        <Header />
      </Router>
    );

    expect(container).toMatchSnapshot();
  });
});
