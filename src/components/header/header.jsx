import React from 'react';
import { Link } from 'react-router-dom';
import Authentication from '../authentication/authentication';
import Container from '../container/container';
import LevelIndicator from '../level-indicator/level-indicator';
import './header.scss';

const Header = () => {
  return (
    <div className="header">
      <Container>
        <header>
          <nav className="header__nav">
            <Link to="/to-do" className="header__logo-link">
              <div className="header__logo">To-do app</div>
            </Link>

            <LevelIndicator />

            <div className="header__auth">
              <Authentication />
            </div>
          </nav>
        </header>
      </Container>
    </div>
  );
};

export default Header;
