import PropTypes from 'prop-types';
import React from 'react';
import firebase from '../../configs/firebase';
import Button from '../button/button';

const LoginButton = ({ title }) => {
  const login = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };

  return (
    <Button color="primary" onClick={login}>
      {title}
    </Button>
  );
};

LoginButton.propTypes = {
  title: PropTypes.string,
};

LoginButton.defaultProps = {
  title: 'Log in',
};

export default LoginButton;
