import { render } from '@testing-library/react';
import React from 'react';
import LoginButton from './login-button';

describe('loginButton', () => {
  it('should render the login button with default text', () => {
    const { container } = render(<LoginButton />);

    expect(container).toMatchInlineSnapshot(`
      <div>
        <button
          class="button button--primary"
          type="button"
        >
          Log in
        </button>
      </div>
    `);
  });

  it('should render the login button with the given title', () => {
    const { container } = render(<LoginButton title="test123" />);

    expect(container).toMatchInlineSnapshot(`
      <div>
        <button
          class="button button--primary"
          type="button"
        >
          test123
        </button>
      </div>
    `);
  });
});
