import { render } from '@testing-library/react';
import React from 'react';
import Spinner from './spinner';

describe('Spinner', () => {
  it('should render the spinner component correctly', () => {
    const { container } = render(<Spinner />);

    expect(container).toMatchSnapshot();
  });

  it('should contain the correct testid', () => {
    const { getByTestId } = render(<Spinner />);

    expect(getByTestId('spinner')).toBeInTheDocument();
  });
});
