import React from 'react';
import './spinner.scss';

const Spinner = () => {
  return <div data-testid="spinner" className="spinner" />;
};

export default Spinner;
