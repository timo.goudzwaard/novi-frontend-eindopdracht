import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Header from './components/header/header';
import './App.scss';
import Homepage from './views/homepage/homepage';
import ToDo from './views/to-do/to-do';
import History from './views/history/history';
import Account from './views/account/account';
import NotFound404 from './views/404/404';

function App() {
  return (
    <div className="app">
      <Header />

      <main>
        <Switch>
          <Route path="/" exact>
            <Homepage />
          </Route>
          <Route path="/to-do">
            <ToDo />
          </Route>
          <Route path="/geschiedenis">
            <History />
          </Route>
          <Route path="/account">
            <Account />
          </Route>
          <Route path="**">
            <NotFound404 />
          </Route>
        </Switch>
      </main>
    </div>
  );
}

export default App;
