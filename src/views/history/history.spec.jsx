import { render } from '@testing-library/react';
import React from 'react';
import History from './history';

describe('History', () => {
  it('should render the component in loading state', () => {
    const { container } = render(<History />);

    expect(container).toMatchSnapshot();
  });

  xit('should render the component in non loading state', () => {});
});
