import React, { useEffect, useState } from 'react';
import Button from '../../components/button/button';
import Card from '../../components/card/card';
import Container from '../../components/container/container';
import ErrorPopUp from '../../components/error-pop-up/error-pop-up';
import Spinner from '../../components/spinner/spinner';
import firebase from '../../configs/firebase';
import './history.scss';

const History = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [history, setHistory] = useState([]);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMesssage] = useState('');

  const onError = (newErrorMessage) => {
    setErrorMesssage(newErrorMessage);
    setError(true);
  };

  useEffect(() => {
    let dbUnsubscribe;

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const db = firebase.firestore();
        dbUnsubscribe = db
          .collection('users')
          .doc(firebase.auth().currentUser.uid)
          .onSnapshot(
            (doc) => {
              // success
              const dbHistory = doc.data().history;

              if (dbHistory.length > 0) {
                setHistory(dbHistory.reverse());
              }
              setIsLoading(false);
            },
            () => {
              // error
              onError(
                'Er is een probleem met de database connectie. Probeer het later nog eens.'
              );
              setIsLoading(false);
            }
          );
      }
    });

    return () => {
      // unsubscribe to database steam
      if (dbUnsubscribe) {
        dbUnsubscribe();
      }
    };
  }, []);

  const handleErrorClose = () => {
    setError(false);
    setErrorMesssage('');
  };

  const renderList = () => {
    return history.map((historyItem) => {
      return (
        <tr
          className="history__tr"
          key={`${historyItem.timestamp}${historyItem.value}`}
        >
          <td>{historyItem.type}</td>
          <td>{historyItem.value}</td>
        </tr>
      );
    });
  };

  const deleteHistory = () => {
    const db = firebase.firestore();
    const userRef = db.collection('users').doc(firebase.auth().currentUser.uid);

    // delete each item in the history
    history.forEach((item) => {
      userRef
        .update({
          history: firebase.firestore.FieldValue.arrayRemove(item),
        })
        .catch(() => {
          onError(
            'Er is een probleem opgetreden met het verwijderen van de geschiedenis.'
          );
        });
    });

    setTimeout(() => {
      setHistory([]);
    }, 100);
  };

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <div className="history">
      <Container>
        <h1>Geschiedenis</h1>

        <h2>Verwijder geschiedenis</h2>
        <Button color="secondary" onClick={deleteHistory}>
          Verwijder
        </Button>

        <h2>Jouw geschiedenis</h2>
        <Card className="history__card">
          {history.length > 0 ? (
            <table className="history__table">
              <thead>
                <tr>
                  <th>type</th>
                  <th>to-do</th>
                </tr>
              </thead>
              <tbody>{renderList()}</tbody>
            </table>
          ) : (
            'Je geschiedenis is leeg'
          )}
        </Card>
      </Container>

      <ErrorPopUp
        hasError={error}
        handleClose={handleErrorClose}
        errorMessage={errorMessage}
      />
    </div>
  );
};

export default History;
