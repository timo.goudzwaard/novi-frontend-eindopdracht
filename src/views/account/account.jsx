import React, { useEffect, useState } from 'react';
import Card from '../../components/card/card';
import Container from '../../components/container/container';
import LevelIndicator from '../../components/level-indicator/level-indicator';
import Spinner from '../../components/spinner/spinner';
import firebase from '../../configs/firebase';
import { calculatePercentageToNextLevel } from '../../utils/calculateExperience';

import './account.scss';

const Account = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [experience, setExperience] = useState(0);

  useEffect(() => {
    let dbUnsubscribe;

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const db = firebase.firestore();
        dbUnsubscribe = db
          .collection('users')
          .doc(firebase.auth().currentUser.uid)
          .onSnapshot(
            (doc) => {
              const dbExperience = doc.data().experience;

              setExperience(dbExperience);
              setIsLoading(false);
            },
            () => {
              setIsLoading(false);
            }
          );
      }
    });

    return () => {
      // unsubscribe to database steam
      if (dbUnsubscribe) {
        dbUnsubscribe();
      }
    };
  }, []);

  const renderExperienceList = () => {
    const experienceList = [
      { experience: 50, text: 'Nieuwe to-do aanmaken' },
      { experience: 250, text: 'Lage prioriteit to-do afvinken' },
      { experience: 750, text: 'Middel prioriteit to-do afvinken' },
      { experience: 2000, text: 'Hoge prioriteit to-do afvinken' },
    ];

    return experienceList.map((item) => {
      return (
        <tr key={item.text}>
          <td>
            {item.experience}
            xp
          </td>
          <td>{item.text}</td>
        </tr>
      );
    });
  };

  if (isLoading) {
    return (
      <div className="account account--loading">
        <Spinner />
      </div>
    );
  }

  return (
    <div className="account">
      <Container>
        <h1>Account</h1>
        <LevelIndicator alignLeft />
        <div>
          {`${calculatePercentageToNextLevel(experience)}% naar volgend level`}
        </div>

        <br />
        <b>Huidige experience</b>
        <div>{experience}</div>

        <h2>Hoe kan je experience verdienen</h2>
        <Card className="account__card">
          <table className="account__table">
            <thead>
              <tr>
                <th>Experience</th>
                <th>Taak</th>
              </tr>
            </thead>
            <tbody>{renderExperienceList()}</tbody>
          </table>
        </Card>
      </Container>
    </div>
  );
};

export default Account;
