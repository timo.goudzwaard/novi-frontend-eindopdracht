import { render } from '@testing-library/react';
import React from 'react';
import Account from './account';

describe('Account', () => {
  it('should render the component as loading', () => {
    const { container } = render(<Account />);

    expect(container).toMatchSnapshot();
  });

  // mock response from firebase to render account page in non loading state
  xit('should render the component without loading state', () => {});
});
