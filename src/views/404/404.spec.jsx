import { render } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import NotFound404 from './404';

describe('NotFound404', () => {
  it('should render the default 404 page', () => {
    const { container } = render(
      <BrowserRouter>
        <NotFound404 />
      </BrowserRouter>
    );

    expect(container).toMatchSnapshot();
  });
});
