import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Button from '../../components/button/button';
import firebase from '../../configs/firebase';
import Container from '../../components/container/container';
import './404.scss';

const NotFound404 = () => {
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        setAuthenticated(true);
      } else {
        setAuthenticated(false);
      }
    });
  });

  const renderButton = () => {
    if (authenticated) {
      return (
        <Link to="/to-do">
          <Button>Terug</Button>
        </Link>
      );
    }

    return (
      <Link to="/">
        <Button>Terug</Button>
      </Link>
    );
  };

  return (
    <div className="not-found-404">
      <Container>
        <h1>404 - Niet gevonden</h1>
        <p>De pagina die je probeert te bezoeken bestaat niet.</p>
        {renderButton()}
      </Container>
    </div>
  );
};

export default NotFound404;
