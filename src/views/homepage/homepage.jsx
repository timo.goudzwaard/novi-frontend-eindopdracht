import React, { useEffect } from 'react';
import './homepage.scss';
import { useHistory } from 'react-router-dom';
import Container from '../../components/container/container';
import LoginButton from '../../components/login-button/login-button';
import Card from '../../components/card/card';
import CardMedia from '../../components/card-media/card-media';
import CardContent from '../../components/card-content/card-content';
import firebase from '../../configs/firebase';
import screenshotToDo from '../../assets/screenshot-to-do.png';
import screenshotHistory from '../../assets/geschiedenis.png';

const Homepage = () => {
  const history = useHistory();

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        history.push('/to-do');
      }
    });
  });

  return (
    <article className="homepage">
      <Container>
        <div className="homepage__hero">
          <h1 className="homepage__title">Welkom bij To-Do app</h1>
          <div className="homepage__cards">
            <Card className="homepage__card">
              <div>
                <CardMedia
                  className="homepage__card-image"
                  image={screenshotToDo}
                  alt="alles in sync"
                />
                <CardContent>
                  <h2>al je apparaten in sync.</h2>
                  <p>
                    Deze to-do lijst maakt gebruik van real-time updates. Wat
                    betekent dat alles continu bijgewerkt wordt.
                  </p>
                  <p>
                    Als je deze app op beide je laptop en telefoon zou openen,
                    zie je direct dat het op beide apparaten bijgewerkt wordt
                    als je iets doet binnen de app.
                  </p>
                  <LoginButton title="Maak account" />
                </CardContent>
              </div>
            </Card>

            <Card className="homepage__card">
              <div>
                <CardMedia
                  className="homepage__card-image"
                  image={screenshotHistory}
                  alt="simpele to-do app"
                />
                <CardContent>
                  <h2>een simpele to-do app.</h2>
                  <p>
                    Het is een vrij simpele to-do app, zonder dat die te veel
                    uitgebreide, onoverzichtelijke opties heeft.
                  </p>
                  <p>
                    Wel heeft het een aantal leuke features zoals gamification,
                    waarmee je experience en levels krijgt als je je to-do lijst
                    netjes afrond. Ook kan je je geschiedenis inzien.
                  </p>
                  <LoginButton title="Maak account" />
                </CardContent>
              </div>
            </Card>
          </div>
        </div>
      </Container>

      <div className="homepage__extra-content-wrapper">
        <Container>
          <div className="homepage__extra-content">
            <h2>Waarom deze app gebruiken?</h2>
            <p>
              Door een to-do lijst met gamification te combineren, maak je taken
              leuker door er een kleine beloning aan te koppelen. Hierdoor wordt
              het leuker en makkelijker om taken te doen, die je normaal zou
              laten liggen.
            </p>
          </div>
        </Container>
      </div>
    </article>
  );
};

export default Homepage;
