import { render } from '@testing-library/react';
import React from 'react';
import HomePage from './homepage';

describe('HomePage', () => {
  it('should match homepage component correctly', () => {
    const { container } = render(<HomePage />);

    expect(container).toMatchInlineSnapshot(`
      <div>
        <article
          class="homepage"
        >
          <div
            class="container"
          >
            <div
              class="homepage__hero"
            >
              <h1
                class="homepage__title"
              >
                Welkom bij To-Do app
              </h1>
              <div
                class="homepage__cards"
              >
                <div
                  class="card homepage__card"
                >
                  <div>
                    <div
                      class="card-media"
                    >
                      <img
                        alt="alles in sync"
                        class="card-media__img"
                        src="screenshot-to-do.png"
                      />
                    </div>
                    <div
                      class="card-content"
                    >
                      <h2>
                        al je apparaten in sync.
                      </h2>
                      <p>
                        Deze to-do lijst maakt gebruik van real-time updates. Wat betekent dat alles continu bijgewerkt wordt.
                      </p>
                      <p>
                        Als je deze app op beide je laptop en telefoon zou openen, zie je direct dat het op beide apparaten bijgewerkt wordt als je iets doet binnen de app.
                      </p>
                      <button
                        class="button button--primary"
                        type="button"
                      >
                        Maak account
                      </button>
                    </div>
                  </div>
                </div>
                <div
                  class="card homepage__card"
                >
                  <div>
                    <div
                      class="card-media"
                    >
                      <img
                        alt="simpele to-do app"
                        class="card-media__img"
                        src="geschiedenis.png"
                      />
                    </div>
                    <div
                      class="card-content"
                    >
                      <h2>
                        een simpele to-do app.
                      </h2>
                      <p>
                        Het is een vrij simpele to-do app, zonder dat die te veel uitgebreide, onoverzichtelijke opties heeft.
                      </p>
                      <p>
                        Wel heeft het een aantal leuke features zoals gamification, waarmee je experience en levels krijgt als je je to-do lijst netjes afrond. Ook kan je je geschiedenis inzien.
                      </p>
                      <button
                        class="button button--primary"
                        type="button"
                      >
                        Maak account
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            class="homepage__extra-content-wrapper"
          >
            <div
              class="container"
            >
              <div
                class="homepage__extra-content"
              >
                <h2>
                  Waarom deze app gebruiken?
                </h2>
                <p>
                  Door een to-do lijst met gamification te combineren, maak je taken leuker door er een kleine beloning aan te koppelen. Hierdoor wordt het leuker en makkelijker om taken te doen, die je normaal zou laten liggen.
                </p>
              </div>
            </div>
          </div>
        </article>
      </div>
    `);
  });
});
