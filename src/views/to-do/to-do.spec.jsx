import { render } from '@testing-library/react';
import React from 'react';
import ToDo from './to-do';

describe('ToDo', () => {
  it('should render the todo component correctly', () => {
    const { container } = render(<ToDo />);

    expect(container).toMatchSnapshot();
  });
});
