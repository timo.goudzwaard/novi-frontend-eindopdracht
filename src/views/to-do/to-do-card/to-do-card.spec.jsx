import { render } from '@testing-library/react';
import React from 'react';
import ToDoCard from './to-do-card';

describe('ToDoCard', () => {
  it('should render the correct card title', () => {
    const { container, getByText } = render(
      <ToDoCard
        isLoading={false}
        card={{ title: 'High Priority', entry: 'highPriority' }}
        onError={() => null}
        handleOpen={() => null}
        currentList={{ highPriority: ['test123', 'test456'] }}
      />
    );

    expect(getByText('High Priority')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('should render the card correctly with items', () => {
    const { container, getByText } = render(
      <ToDoCard
        isLoading={false}
        card={{ title: 'test', entry: 'highPriority' }}
        onError={() => null}
        handleOpen={() => null}
        currentList={{ highPriority: ['test123', 'test456'] }}
      />
    );

    expect(getByText('test123')).toBeInTheDocument();
    expect(getByText('test456')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('should render the card correctly with NO items', () => {
    const { getByText, container } = render(
      <ToDoCard
        isLoading={false}
        card={{ title: 'test', entry: 'test123' }}
        onError={() => null}
        handleOpen={() => null}
        currentList={{}}
      />
    );

    expect(getByText('Deze lijst is (nog) leeg.')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });

  it('should render the card with loading spinner', () => {
    const { getByTestId, container } = render(
      <ToDoCard
        isLoading
        card={{ title: 'test', entry: 'test123' }}
        onError={() => null}
        handleOpen={() => null}
        currentList={{}}
      />
    );

    expect(getByTestId('spinner')).toBeInTheDocument();
    expect(container).toMatchSnapshot();
  });
});
