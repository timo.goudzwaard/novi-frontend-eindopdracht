import PropTypes from 'prop-types';
import React from 'react';
import Button from '../../../components/button/button';
import Card from '../../../components/card/card';
import CardContent from '../../../components/card-content/card-content';
import Spinner from '../../../components/spinner/spinner';
import firebase from '../../../configs/firebase';
import './to-do-card.scss';
import ErrorMessages from '../../../configs/error-messages';

const ToDoCard = ({ card, isLoading, currentList, onError, handleOpen }) => {
  const removeItem = (item, entry) => {
    const db = firebase.firestore();
    const userRef = db.collection('users').doc(firebase.auth().currentUser.uid);
    const baseExperience = {
      lowPriority: 250,
      middlePriority: 750,
      highPriority: 2000,
    };

    // remove item from the list. the timeout is to let the user see the checkbox checked before it's removed from the list.
    setTimeout(() => {
      userRef
        .update({
          [entry]: firebase.firestore.FieldValue.arrayRemove(item),
        })
        .catch(() => {
          onError(ErrorMessages.checkToDo);
        });

      // add to history
      userRef
        .update({
          history: firebase.firestore.FieldValue.arrayUnion({
            type: 'afgevinkt',
            priority: card.title,
            value: item,
            timestamp: new Date(),
          }),
        })
        .catch(() => {
          onError(ErrorMessages.addHistory);
        });

      // add experience
      userRef
        .update({
          experience: firebase.firestore.FieldValue.increment(
            baseExperience[entry]
          ),
        })
        .catch(() => {
          onError(ErrorMessages.addXP);
        });
    }, 300);
  };

  const renderList = (entry) => {
    if (currentList && !isLoading) {
      if (!currentList[entry] || currentList[entry].length === 0) {
        return 'Deze lijst is (nog) leeg.';
      }

      if (currentList[entry]) {
        return currentList[entry].map((item) => {
          return (
            <li className="to-do-card__item" key={item}>
              {item}
              <input
                type="checkbox"
                className="to-do-card__checkbox"
                onClick={() => {
                  removeItem(item, entry);
                }}
              />
            </li>
          );
        });
      }
    }

    return <Spinner />;
  };

  return (
    <Card className="to-do-card">
      <CardContent>
        <h2>{card.title}</h2>
        <ul className="to-do-card__list">{renderList(card.entry)}</ul>
        <Button
          color="tertiary"
          onClick={() => {
            handleOpen(card);
          }}
        >
          +
        </Button>
      </CardContent>
    </Card>
  );
};

ToDoCard.propTypes = {
  card: PropTypes.shape({
    title: PropTypes.string,
    entry: PropTypes.string,
  }).isRequired,
  isLoading: PropTypes.bool.isRequired,
  currentList: PropTypes.shape({
    lowPriority: PropTypes.arrayOf(PropTypes.string),
    middlePriority: PropTypes.arrayOf(PropTypes.string),
    highPriority: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
  onError: PropTypes.func.isRequired,
  handleOpen: PropTypes.func.isRequired,
};

export default ToDoCard;
