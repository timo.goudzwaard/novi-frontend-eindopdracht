import React, { useEffect, useState } from 'react';
import Button from '../../components/button/button';
import CardContent from '../../components/card-content/card-content';
import Card from '../../components/card/card';
import Container from '../../components/container/container';
import ErrorPopUp from '../../components/error-pop-up/error-pop-up';
import Modal from '../../components/modal/modal';
import TextField from '../../components/text-field/text-field';
import firebase from '../../configs/firebase';
import ToDoCard from './to-do-card/to-do-card';
import ErrorMessages from '../../configs/error-messages';
import './to-do.scss';

const ToDo = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [open, setOpen] = useState(false);
  const [priorityTitle, setPriorityTitle] = useState('');
  const [priorityEntry, setPriorityEntry] = useState('');
  const [newItem, setNewItem] = useState('');
  const [currentList, setCurrentList] = useState({});
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleOpen = (priority) => {
    setPriorityTitle(priority.title);
    setPriorityEntry(priority.entry);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onError = (newErrorMessage) => {
    setErrorMessage(newErrorMessage);
    setError(true);
  };

  const handleErrorClose = () => {
    setErrorMessage('');
    setError(false);
  };

  const addItem = () => {
    const db = firebase.firestore();
    const userRef = db.collection('users').doc(firebase.auth().currentUser.uid);

    // add item to list
    userRef
      .update({
        [priorityEntry]: firebase.firestore.FieldValue.arrayUnion(newItem),
      })
      .catch(() => {
        onError(ErrorMessages.addToDo);
      });

    // add to history
    userRef
      .update({
        history: firebase.firestore.FieldValue.arrayUnion({
          type: 'toegevoegd',
          priority: priorityEntry,
          value: newItem,
          timestamp: new Date(),
        }),
      })
      .catch(() => {
        onError(ErrorMessages.addHistory);
      });

    // add experience
    userRef
      .update({
        experience: firebase.firestore.FieldValue.increment(50),
      })
      .catch(() => {
        onError(ErrorMessages.addXP);
      });

    // reset item
    setNewItem('');

    // close modal
    setOpen(false);
  };

  useEffect(() => {
    let dbUnsubscribe;

    // check if user exists in the database, if not, create and add initial values
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const db = firebase.firestore();
        dbUnsubscribe = db
          .collection('users')
          .doc(firebase.auth().currentUser.uid)
          .onSnapshot(
            (doc) => {
              if (!doc.data()) {
                const listRef = db
                  .collection('users')
                  .doc(firebase.auth().currentUser.uid);

                // set initial values
                listRef.set({
                  dateCreated: new Date(),
                  experience: 0,
                  history: [],
                  highPriority: [],
                  middlePriority: [],
                  lowPriority: [],
                });
              }

              setCurrentList(doc.data());
              setIsLoading(false);
            },
            () => {
              onError(ErrorMessages.initialize);
              setIsLoading(false);
            }
          );
      }
    });

    return () => {
      // unsubscribe to database steam
      if (dbUnsubscribe) {
        dbUnsubscribe();
      }
    };
  }, []);

  const renderCards = () => {
    const cards = [
      { title: 'hoge prioriteit', entry: 'highPriority' },
      { title: 'middel prioriteit', entry: 'middlePriority' },
      { title: 'lage prioriteit', entry: 'lowPriority' },
    ];

    return cards.map((card) => {
      return (
        <ToDoCard
          key={card.entry}
          card={card}
          isLoading={isLoading}
          currentList={currentList}
          onError={onError}
          handleOpen={handleOpen}
        />
      );
    });
  };

  return (
    <article className="to-do">
      <Container>
        <h1>Jouw To-do Lijst</h1>

        <div className="to-do__cards">{renderCards()}</div>

        <Modal open={open} onClose={handleClose}>
          <Card className="to-do__modal-card">
            <CardContent>
              <h2 className="to-do__modal-title">{priorityTitle}</h2>
              <TextField
                id="new-to-do"
                label="Voeg een nieuw to-do item toe"
                className="to-do__text-field"
                value={newItem}
                onChange={(event) => {
                  setNewItem(event.target.value);
                }}
              />
              <div className="to-do__modal-buttons">
                <Button onClick={handleClose} color="secondary">
                  x
                </Button>
                <Button onClick={addItem} color="tertiary">
                  Aanmaken
                </Button>
              </div>
            </CardContent>
          </Card>
        </Modal>
      </Container>

      {/* show snackbar for error messages */}
      <ErrorPopUp
        hasError={error}
        handleClose={handleErrorClose}
        errorMessage={errorMessage}
      />
    </article>
  );
};

export default ToDo;
